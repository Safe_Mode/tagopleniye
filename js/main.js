'use strict';

var $ = require('../node_modules/jquery/dist/jquery');
var Masonry = require('masonry-layout');
var initTabs = require('./app/init-tabs');
var activateForm = require('./app/activate-form');
var accordeon = require('./app/accordeon');
var showModal = require('./app/modal');

require('malihu-custom-scrollbar-plugin')($);
require('./libs/number');
require('./app/init-sliders');
require('./app/map');

var Width = {
  Tablet: {
    S: 480,
    M: 768,
    L: 992
  },
  LAPTOP: 1024
};

initTabs('.catalog-item__tech');

if (window.screen.width >= Width.Tablet.M) {
  initTabs('.relative');
}

var togglerEl = $('.catalog-item__toggler');
var togglerDefaultHeight = $(togglerEl).find('p').outerHeight(true);

$(togglerEl).height(togglerDefaultHeight);

var setNewHeight = function (element, defaultHeight) {
  var paragraphEl = $(element).find('p');
  var togglerHeight = $(paragraphEl).outerHeight(true) * $(paragraphEl).length;
  var finalHeight;

  if ($(element).outerHeight() === togglerHeight) {
    finalHeight = defaultHeight;
  } else {
    finalHeight = togglerHeight;
  }

  $(element).animate({
    height: finalHeight
  });
};

var expandEl = $('.catalog-item__expand');
var closeText = $(expandEl).text();

var toggleText = function (target, defaultText) {
  var openText = 'Скрыть подробности';

  if ($(target).text() === openText) {
    $(target).text(defaultText);
  } else {
    $(target).text(openText);
  }
};

$(expandEl).click(function (evt) {
  evt.preventDefault();

  setNewHeight(togglerEl, togglerDefaultHeight);
  toggleText(evt.target, closeText);
});

$('.number__input').each(function (i, it) {
  $(it).number({
    'containerClass': 'cart__quantity number',
    'minus': 'number__btn number__btn--down',
    'plus': 'number__btn number__btn--up',
    'containerTag': 'p',
    'btnTag': 'button'
  });
});

var editPersonalEl = $('.personal__link--edit');
var personalFormEl = $('.personal__form');
var editRequisitesEl = $('.requisites__edit');
var requisitesForm = $('.requisites__form');

var toggleControls = function () {
  $('.personal__controls').toggle();
};

activateForm({
  control: editPersonalEl,
  form: personalFormEl,
  cb: toggleControls
});

activateForm({
  control: editRequisitesEl,
  form: requisitesForm
});

accordeon({
  container: '.table__content.accordeon',
  closeRest: true,
  activeClass: 'accordeon__toggle--active'
});

if (window.screen.width < Width.LAPTOP) {
  accordeon({
    container: '.form__wrapper.accordeon',
    closeRest: true,
    activeClass: 'accordeon__toggle--active'
  });
}

showModal({
  control: '.menu__item--open',
  modal: '.modal--menu',
  overlay: false,
  animate: true,
  side: 'left'
});

showModal({
  control: '.menu__item--search',
  modal: '.modal--search',
  overlay: false,
  animate: true,
  side: 'left'
});

var modalCartSide = 'right';

if (window.screen.width >= Width.LAPTOP) {
  modalCartSide = 'left';
}

showModal({
  control: '.menu__item--cart',
  modal: '.modal--cart',
  overlay: false,
  animate: false,
  side: modalCartSide
});

showModal({
  control: '.page-header__toggler',
  modal: '.modal--filter',
  overlay: false,
  animate: true,
  side: 'right'
});

var loginToggleSelector;

if (window.screen.width <= Width.Tablet.M) {
  loginToggleSelector = '.main-nav__item--login .main-nav__link';
} else {
  loginToggleSelector = '.menu__item--login';
}

showModal({
  control: loginToggleSelector,
  modal: '.modal--login',
  overlay: true,
  animate: true,
  side: 'top'
});

showModal({
  control: '.modal__link--reg',
  modal: '.modal--reg',
  overlay: true,
  animate: true,
  side: 'top'
});

showModal({
  control: '.personal__controls',
  modal: '.modal--pass',
  overlay: true,
  animate: true,
  side: 'top'
});

accordeon({
  container: '.order__delivery .order__list',
  closeRest: true,
  default: true,
  activeClass: 'accordeon__toggle--active'
});

accordeon({
  container: '.order__payment .order__list',
  closeRest: true,
  default: true,
  activeClass: 'accordeon__toggle--active'
});

showModal({
  control: '.requisites__btn',
  modal: '.modal--address',
  overlay: true,
  animate: true,
  side: 'top'
});

if (window.screen.width >= Width.Tablet.M
    && window.screen.width < Width.LAPTOP
    && $('.news').get(0)) {
  var msnry = new Masonry('.news', {
    itemSelector: '.novelty',
    percentPosition: true,
    columnWidth: '.grid-sizer'
  });
}

if ($('.news').get(0) && window.screen.width >= Width.LAPTOP) {
  $(window).on('load', function () {
    $('.page-main').mCustomScrollbar({
      theme: 'page-content',
      axis: 'x',
      advanced: {
        autoExpandHorizontalScroll: true
      },
      mouseWheel: {
        deltaFactor: 1000
      }
    });
  });
}

$('.contact__title').click(function (evt) {
  evt.preventDefault();
  $('.contact__hidden').slideToggle();
});
