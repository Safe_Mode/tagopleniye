'use strict';

var $ = require('../../node_modules/jquery/dist/jquery');

$.fn.number = function (customOptions) {
  var options = {
    'containerClass': 'number-style',
    'minus': 'number-minus',
    'plus': 'number-plus',
    'containerTag': 'div',
    'btnTag': 'span'
  };

  options = $.extend(true, options, customOptions);

  var input = this;

  input.wrap('<' + options.containerTag + ' class="' + options.containerClass + '">');

  var wrapper = input.parent();

  wrapper.prepend('<' + options.btnTag + ' class="' + options.minus + '"></' + options.btnTag + '>');
  wrapper.append('<' + options.btnTag + ' class="' + options.plus + '"></' + options.btnTag + '>');

  var minusClass = options.minus.slice(options.minus.indexOf(' ') + 1);
  var plusClass = options.plus.slice(options.plus.indexOf(' ') + 1);
  var minus = wrapper.find('.' + minusClass);
  var plus = wrapper.find('.' + plusClass);

  if (options.btnTag.toLowerCase() === 'button') {
    minus.attr('type', 'button');
    plus.attr('type', 'button');
  }

  var min = input.attr('min');
  var max = input.attr('max');
  var step;

  if (input.attr('step')) {
    step = +input.attr('step');
  } else {
    step = 1;
  }

  if (+input.val() <= +min) {
    minus.addClass('disabled');
  }

  if (+input.val() >= +max) {
    plus.addClass('disabled');
  }

  minus.click(function (evt) {
    evt.preventDefault();

    input = $(evt.target).parent().find('input');

    var value = input.val();

    if (+value > +min || !min) {
      input.val(+value - step);

      if (+input.val() === +min) {
        input.prev('.' + minusClass).addClass('disabled');
      }

      if (input.next('.' + plusClass).hasClass('disabled')) {
        input.next('.' + plusClass).removeClass('disabled');
      }

    }

  });

  plus.click(function (evt) {
    evt.preventDefault();

    input = $(evt.target).parent().find('input');

    var value = input.val();

    if (+value < +max || !max) {
      input.val(+value + step);

      if (+input.val() === +max) {
        input.next('.' + plusClass).addClass('disabled');
      }

      if (input.prev('.' + minusClass).hasClass('disabled')) {
        input.prev('.' + minusClass).removeClass('disabled');
      }

    }

  });
};
