'use strict';

(function () {
  var lastTimeout;

  module.exports = function (callback, interval) {
    if (lastTimeout) {
      window.clearTimeout(lastTimeout);
    }

    lastTimeout = window.setTimeout(callback, interval);
  };
})();
