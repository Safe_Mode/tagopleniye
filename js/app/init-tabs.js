'use strict';

var $ = require('../../node_modules/jquery/dist/jquery');
var relativeSwiper = require('./init-sliders').relativeSwiper;

module.exports = function (tabs) {
  var tabTogglesEl = $(tabs).find('.tabs__toggle');
  var tabContentItems = $(tabs).find('.tabs__item');
  var activeTabClass = 'tabs__toggle--active';
  var activeContentClass = 'tabs__item--active';

  $(tabs).find('.tabs__toggles').click(function (evt) {
    evt.preventDefault();

    var index;
    var activeContentEl = $(tabs).find('.' + activeContentClass);

    if (!$(evt.target).hasClass(activeTabClass)) {
      index = $(tabTogglesEl).index(evt.target);

      $(tabTogglesEl).each(function (i, it) {
        if ($(it).hasClass(activeTabClass)) {
          $(it).removeClass(activeTabClass);
        }
      });

      $(evt.target).addClass(activeTabClass);

      $(activeContentEl).fadeOut();
      $(activeContentEl).promise().done(function () {
        $(tabContentItems[index]).fadeIn();
        $(activeContentEl).removeClass(activeContentClass);
        $(tabContentItems[index]).addClass(activeContentClass);

        $(relativeSwiper).each(function (i, it) {
          it.update();
        });
      });
    }
  });
};
