'use strict';

var Swiper = require('./../../node_modules/swiper/dist/js/swiper');
var baguette = require('./../../node_modules/baguettebox.js/dist/baguetteBox');

var Width = {
  Tablet: {
    S: 480,
    M: 768,
    L: 992
  },
  LAPTOP: 1024,
  Desktop: {
    S: 1200
  }
};

var reviewsSwiper = new Swiper('.reviews', {
  spaceBetween: 25,
  pagination: {
    el: '.reviews__pagination',
    bulletClass: 'reviews__bullet',
    bulletActiveClass: 'reviews__bullet--active',
    clickable: true
  }
});

var vendorSlidesCount = 2;

if (window.screen.width >= Width.Tablet.L) {
  vendorSlidesCount = 5;
} else if (window.screen.width >= Width.Tablet.M) {
  vendorSlidesCount = 4;
} else if (window.screen.width >= Width.Tablet.S) {
  vendorSlidesCount = 3;
}

var vendorsSwiper = new Swiper('.vendors', {
  slidesPerView: vendorSlidesCount,
  scrollbar: {
    el: '.vendors__scroll',
    draggable: true,
    dragClass: 'vendors__drag'
  }
});

if (window.screen.width < Width.LAPTOP) {
  var pageNavSwiper = new Swiper('.page-nav__wrapper', {
    slidesPerView: 'auto'
  });
}

var thumbsSlidesCount = 3;
var thumbsDirection = 'horizontal';

if (window.screen.width >= Width.LAPTOP) {
  thumbsSlidesCount = 3;
  thumbsDirection = 'vertical';
} else if (window.screen.width >= Width.Tablet.L) {
  thumbsSlidesCount = 5;
} else if (window.screen.width >= Width.Tablet.S) {
  thumbsSlidesCount = 4;
}

var gallerySwiper = new Swiper('.preview', {});
var thumbsSwiper = new Swiper('.thumbs__wrapper', {
  direction: thumbsDirection,
  slidesPerView: thumbsSlidesCount,
  slideActiveClass: 'thumbs__item--active',
  navigation: {
    prevEl: '.thumbs__btn--prev',
    nextEl: '.thumbs__btn--next',
    disabledClass: 'thumbs__btn--disabled'
  },
  on: {
    init: function () {
      this.slides[gallerySwiper.activeIndex].classList.add('thumbs__item--selected');
    },
    tap: function () {
      gallerySwiper.slideTo(this.clickedIndex);

      this.slides.forEach = [].forEach;
      this.slides.forEach(function (item) {
        if (item.classList.contains('thumbs__item--selected')) {
          item.classList.remove('thumbs__item--selected');
        }
      });
      this.clickedSlide.classList.add('thumbs__item--selected');
    }
  }
});

baguette.run('.preview__list');
baguette.run('.news');

var relativeSlidesCount = 1.2;
var relativeScrollbar = null;

if (window.screen.width >= Width.Desktop.S) {
  relativeSlidesCount = 4;
} else if (window.screen.width >= Width.Tablet.M) {
  relativeSlidesCount = 3;
} else if (window.screen.width >= Width.Tablet.S) {
  relativeSlidesCount = 2;
}

if (window.screen.width >= Width.LAPTOP) {
  relativeScrollbar = '.relative__scrollbar';
}

exports.relativeSwiper = new Swiper('.relative__slider', {
  slidesPerView: relativeSlidesCount,
  scrollbar: {
    el: relativeScrollbar,
    draggable: true,
    dragClass: 'relative__drag'
  }
});
