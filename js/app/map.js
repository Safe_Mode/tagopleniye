'use strict';

var $ = require('../../node_modules/jquery/dist/jquery');

ymaps.ready(function () {
  var SVG_ENDPOINT = './img/svg/';
  var Coords = {
    FIRST: [47.2225, 38.9073],
    SECOND: [47.2137, 38.9072],
    THIRD: [47.2696, 38.9078]
  };

  var setAddressToBalloon = function (placemark) {
    return ymaps.geocode(placemark.geometry._coordinates).then(function (res) {
      var firstGeoObject = res.geoObjects.get(0);

      placemark.properties.set({
        balloonContent: firstGeoObject.getAddressLine()
      });

      return firstGeoObject.getAddressLine();
    });
  };

  var getMapCenter = function (map) {
    var mapCenter = [];

    map.getCenter().forEach(function (coord) {
      mapCenter.push(Math.round(coord * 10000) / 10000);
    });

    return mapCenter;
  };

  var setPassivePlacemarkState = function (array) {
    array.each(function (placemark) {

      placemark.options.set({
        iconImageHref: SVG_ENDPOINT + 'map-marker.svg'
      });
    });
  };

  var setActivePlacemarkState = function (placemark, map) {
    var contactMapCenter = getMapCenter(map);

    if (placemark.geometry._coordinates.every(function (coord, i) {
      return coord === contactMapCenter[i];
    })) {
      placemark.options.set({
        iconImageHref: SVG_ENDPOINT + 'map-marker-active.svg'
      });
    }
  };

  if ($('#delivery-map').get(0)) {
    var deliveryMap = new ymaps.Map('delivery-map', {
      center: [47.2225, 38.9073],
      zoom: 17,
      controls: []
    });

    var deliveryMark = new ymaps.Placemark(Coords.FIRST, {}, {
      iconLayout: 'default#image',
      iconImageHref: SVG_ENDPOINT + 'map-marker.svg',
      iconImageSize: [30, 40]
    });

    deliveryMap.geoObjects.add(deliveryMark);
  }

  if ($('#contact-map').get(0)) {
    var contactMap = new ymaps.Map('contact-map', {
      center: Coords.FIRST,
      zoom: 17,
      controls: []
    });

    var contactMarks = new ymaps.GeoObjectCollection({}, {
      iconLayout: 'default#image',
      iconImageHref: SVG_ENDPOINT + 'map-marker.svg',
      iconImageSize: [30, 40]
    });

    var $links = $('.contact__link');

    $links.get(0).focus();
    contactMarks.add(new ymaps.Placemark(Coords.FIRST));
    contactMarks.add(new ymaps.Placemark(Coords.SECOND));
    contactMarks.add(new ymaps.Placemark(Coords.THIRD));

    contactMarks.each(function (placemark) {
      $.when(setAddressToBalloon(placemark)).done(function (res) {
        $($links[contactMarks.indexOf(placemark)]).data('address', res);
      });

      setActivePlacemarkState(placemark, contactMap);
    });

    $links.click(function (evt) {
      evt.preventDefault();

      contactMarks.each(function (placemark) {
        if ($(evt.currentTarget).data().address === placemark.properties.get('balloonContent')) {
          contactMap.panTo(placemark.geometry._coordinates).then(function () {
            setPassivePlacemarkState(contactMarks);
            setActivePlacemarkState(placemark, contactMap);
          });
        }
      });
    });

    contactMap.geoObjects.add(contactMarks);
  }
});
