'use strict';

var DEBOUNCE_TIME = 0;

var $ = require('../../node_modules/jquery/dist/jquery');
var debounce = require('./debounce');

module.exports = function (props) {
  var accordeonToggleEl = $(props.container).find('.accordeon__toggle');
  var accordeonDropdownEl = $(props.container).find('.accordeon__dropdown');

  accordeonToggleEl.click(function (evt) {
    if (!props.default) {
      evt.preventDefault();
    }

    var index = $(accordeonToggleEl).index(evt.currentTarget);

    if (props.closeRest) {
      accordeonDropdownEl.each(function (i, it) {
        if (i !== index) {
          $(it).slideUp();
        }
      });
    }

    if (props.activeClass) {
      accordeonToggleEl.each(function (i, it) {
        if (it !== evt.currentTarget) {
          $(it).removeClass(props.activeClass);
        }
      });

      $(evt.currentTarget).toggleClass(props.activeClass);
    }

    debounce(function () {
      $(accordeonDropdownEl[index]).slideToggle();
    }, DEBOUNCE_TIME);
  });
};
