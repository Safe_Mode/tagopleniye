'use strict';

var $ = require('../../node_modules/jquery/dist/jquery');

var Width = {
  Tablet: {
    S: 480,
    M: 768,
    L: 992
  },
  LAPTOP: 1024
};

var TOGGLE_TIME = 700;

module.exports = function (props) {
  var $openEl = $(props.control);
  var $modalEl = $(props.modal);
  var $closeEl = $modalEl.find('.modal__close');

  if (props.overlay) {
    var $overlayEl = $('.overlay');
  }

  var toggleModal = function (modal, side) {
    if (window.screen.width < Width.Tablet.M) {
      modal.fadeToggle();
    } else {
      var position = (modal.css(side) === '0px') ? '-100%' : 0;
      var time = props.animate ? TOGGLE_TIME : 0;

      if (side === 'left') {
        modal.animate({
          left: position
        }, time);
      } else if (side === 'top') {
        modal.animate({
          top: position
        }, time);
      } else {
        modal.animate({
          right: position
        }, time);
      }
    }
  };

  var openModal = function (modal, side) {
    toggleModal(modal, side);

    if ($overlayEl) {
      $overlayEl.fadeIn(TOGGLE_TIME);
    }

    $closeEl.click(onCloseClick);
  };

  var closeModal = function (modal, side) {
    toggleModal(modal, side);
    $closeEl.off('click', onCloseClick);

    if ($overlayEl) {
      $overlayEl.fadeOut(TOGGLE_TIME);
      $overlayEl.off('click', onOverlayClick);
    }
  };

  var closePrev = function () {
    $('.modal').each(function (index, item) {
      if (window.screen.width < Width.Tablet.M && $(item).css('display') !== 'none') {
        closeModal($(item));
      } else {
        var position = ['top', 'right', 'left'];

        position.forEach(function (it) {
          if ($(item).css(it) === item.style[it]) {
            closeModal($(item), it);
          }
        });
      }
    });
  };

  var onOpenClick = function (evt) {
    evt.preventDefault();

    $.when(closePrev()).done(function () {
      openModal($modalEl, props.side);
    });
  };

  var onCloseClick = function (closeEvt) {
    closeEvt.preventDefault();
    closeModal($modalEl, props.side);
  };

  var onOverlayClick = function (overEvt) {
    overEvt.preventDefault();
    closeModal($modalEl, props.side);
  };

  $openEl.click(onOpenClick);

  if ($overlayEl) {
    $overlayEl.click(onOverlayClick);
  }
};
