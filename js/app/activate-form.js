'use strict';

var $ = require('../../node_modules/jquery/dist/jquery');

module.exports = function (props) {
  props.control.click(function (evt) {
    evt.preventDefault();

    $(props.form.find('input')).each(function (i, it) {
      it.disabled = false;
    });

    $(props.form.find('button[type="submit"]')).show();

    if (props.cb) {
      props.cb();
    }
  });
};
